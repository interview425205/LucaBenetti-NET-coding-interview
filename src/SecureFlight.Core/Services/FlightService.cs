﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class FlightService : BaseService<Flight>, IFlightService
    {
        private readonly IRepository<Passenger> _passengerRepository;

        public FlightService(IRepository<Flight> repository, IRepository<Passenger> passengerRepository) : base(repository)
        {
            _passengerRepository = passengerRepository;
        }

        public async Task<OperationResult<Flight>> AddPassenger(long flightId, string passengerId)
        {
            var flight = (await _repository.FilterAsync(x => x.Id == flightId)).FirstOrDefault();

            if (flight == null)
            {
                throw new ArgumentException("Flight not found");
            }

            var passenger = (await _passengerRepository.FilterAsync(x => x.Id == passengerId)).FirstOrDefault();

            if (passenger == null)
            {
                throw new ArgumentException("Passenger not found");
            }

            flight.Passengers.Add(passenger);
            return await PutAsync(flight);
        }

        public async Task<OperationResult<IReadOnlyList<Flight>>> GetByOriginAndDestination(string originAirport, string destinationAirport)
        {
            return new OperationResult<IReadOnlyList<Flight>>(await _repository.FilterAsync(x => x.OriginAirport == originAirport && x.DestinationAirport == destinationAirport));
        }
    }
}
