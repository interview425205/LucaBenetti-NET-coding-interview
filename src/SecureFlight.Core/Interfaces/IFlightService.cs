﻿using SecureFlight.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IFlightService : IService<Flight>
    {
        Task<OperationResult<Flight>> AddPassenger(long flightId, string passengerId);
        Task<OperationResult<IReadOnlyList<Flight>>> GetByOriginAndDestination(string originAirport, string destinationAirport);
    }
}
